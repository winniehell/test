$`a in A`$
$`a \in A`$

`abcnop`
`abc\nop`

$`a \in A`$ and $`b \in B`$ but also $`c = a`$ and $`c = b`$
